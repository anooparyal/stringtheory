see http://www.fretjam.com/guitar-chord-theory.html

Major:
1 3 5

Minor:
1 b3 5

Sus2:
1 2 5

Sus4:
1 4 5

Aug (major, sharp 5):
1 3 #5

Dim (minor, flat 5):
1 b3 b5

Maj7
1 3 5 7

dom7
1 3 5 b7

m7
1 b3 5 b7

mM7
1 b3 5 7

aug7
1 3 #5 b7

half dim7 (m7 b5)
1 b3 b5 b7

dim7
1 b3 b5 bb7

sus7
1 2/4 5 b7

